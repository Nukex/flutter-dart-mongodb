import 'dart:io';
import 'dart:convert';

import 'package:mongo_dart/mongo_dart.dart';

import 'package:dartmongo/dartmongo.dart' as dartmongo;

main(List<String> arguments) async {
  int port = 8085;
  var server = await HttpServer.bind('192.168.0.9', port);
  Db db = Db('mongodb://127.0.0.1:27017/test');
  await db.open();

  print('Conectado com o Banco de Dados');

  DbCollection coll = db.collection('numbers');

  server.listen((HttpRequest request) async {
    if (request.uri.path == '/') {
      // Lidar com a requisicao GET
      if (request.method == 'GET') {
        request.response.write(await coll.find().toList());
      }
      // Lidar com a requisicao POST
      else if (request.method == 'POST') {
        var content = await utf8.decoder.bind(request).join();
        var document = json.decode(content);
        await coll.save(document);
      }
      // Lidar com a requisicao PUT
      else if (request.method == 'PUT') {
        var id = int.parse(request.uri.queryParameters['id']);
        var content = await utf8.decoder.bind(request).join();
        var document = json.decode(content);
        var itemToReplace = await coll.findOne(where.eq('id', id));

        if (itemToReplace == null) {
          await coll.save(document);
        } else {
          await coll.update(itemToReplace, document);
        }
      }
      // Lidar com a requisicao DELETE
      else if (request.method == 'DELETE') {
        var id = int.parse(request.uri.queryParameters['id']);
        var itemToDelete = await coll.findOne(where.eq('id', id));
        await coll.remove(itemToDelete);
      }
      // Lidar com a requisicao PATCH
      else if (request.method == 'PATCH') {
        var id = int.parse(request.uri.queryParameters['id']);
        var content = await utf8.decoder.bind(request).join();
        var document = json.decode(content);
        var itemToPatch = await coll.findOne(where.eq('id', id));
        await coll.update(itemToPatch, {
          r'$set': document,
        });
      }
      await request.response.close();
    } else {
      request.response
        ..statusCode = HttpStatus.notFound
        ..write('Nao Encontrado');
      await request.response.close();
    }
  });

  print('Servidor ouvindo em http://192.168.0.9:$port');
}
